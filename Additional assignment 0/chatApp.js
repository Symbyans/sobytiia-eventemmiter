const EventEmitter = require('events');

class ChatApp extends EventEmitter {

  constructor(title) {
    super();

    this.title = title;

    setInterval(() => {
      this.emit('message', `${this.title}: ping-pong`);
    }, 1000);
  }

  close(){
    vkChat.emit('close');
  }
}


 

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');



let chatOnMessage = (message) => {
  console.log(message);         
};

let readyToAnswer = (message) =>{
  console.log('Готовлюсь к ответу');
};




vkChat.getMaxListeners(2);

webinarChat.on('message', chatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);

webinarChat.on('message', readyToAnswer);
vkChat.on('message', readyToAnswer);
vkChat.on('close',function() {console.log('Чат вконтакте закрылся :(')});


module.exports={
  chatOnMessage,
  webinarChat,
  facebookChat,
  vkChat
}






